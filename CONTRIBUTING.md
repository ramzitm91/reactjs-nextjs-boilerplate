# Contributing

## Pull Request Process

1. Create a feature branch out of latest `master` branch.

```
git checkout -b origin/master develop-xxx
```

2. Hack away!
3. When you are ready to show your work, push the feature branch to origin.

```
git push -u origin develop-feature-branch-xxx
```

4. Create a pull request targeting `master` branch.
    * If there is a Trello ticket, add the ticket identifier/url to the description.
    * If the branch is still in-progress, tag the PR with `in-progress` tag.
    * If there is no more development left in the branch, tag the PR with `review` tag.
5. As the pull request maker, you are responsible for managing the pull request, which includes:
    * Add appropriate description and title to the pull request.
    * Merge the pull request once there are approvals.
    * Lastly, if there is no approval yet, please notify the reviewers.
    * You can merge your PR if there is no approval from reviewers in 2 working days.
6. Thank you for the contribution!
