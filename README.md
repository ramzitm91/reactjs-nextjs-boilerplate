# ReactJS x NextJS Boilerplate

## Getting Started
#### Development
Install Dependencies
```
npm install
```
Run on your local machine
```
npm run dev
```
Open `http://localhost:3000`

#### Coding Standard
- [standardjs](https://standardjs.com/)

#### Contributing
- See [Contributing](CONTRIBUTING.md)