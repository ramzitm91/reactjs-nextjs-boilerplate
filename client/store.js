import { createStore, applyMiddleware, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import reducers from './reducers'
import actions from './actions'

export function mapStateToProps (...keys) {
  return (state) => keys.reduce((res, key) => {
    res[key] = state[key]
    return res
  }, {})
}

export function mapActions (...keys) {
  return (dispatch) => keys.reduce((res, key) => {
    if (typeof actions[key] === 'function') {
      res[key] = (...args) => actions[key](...args)(dispatch)
      return res
    }
  }, {})
}

export function initializeStore (initialState = {}) {
  const reducer = combineReducers({ ...reducers })
  return createStore(reducer, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)))
}
