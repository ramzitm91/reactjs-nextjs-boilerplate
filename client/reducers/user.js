export const user = (state = {}, action) => {
  if (action.type === 'SET_USER') {
    if (action?.data?.code === 200) {
      state = action.data.data
    }
  }
  return state
}

export const isLogin = (state = false, action) => {
  if (action.type === 'SET_LOGIN') {
    state = true
    window.localStorage.setItem('bo_bearer', action?.data?.data?.token)
  } else if (action.type === 'SET_LOGOUT') {
    state = false
    window.localStorage.removeItem('bo_bearer')
    console.log('done')
  }
  return state
}
