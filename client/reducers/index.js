/**
 * import all variables within reducers
 */
import * as user from './user'

export default {
  ...user
}
