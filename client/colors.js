module.exports = {
  primary: '#93cbf1',
  secondary: '#ffe495',
  third: '#ffb295',
  danger: '#D01C25',
  warning: '#af812d',

  primaryDarker: '#55a3d8',

  handphone: '#bd1f2d',
  facebook: '#2072f9',
  twitter: '#2d9ef8',
  google: '#D0161B',

  transparent: 'transparent'
}
