export const genreToString = (genre = []) => {
  return genre.map((v) => v.name).join(', ')
}

/**
 * format minute into string like 1h 10m
 * @param {*} minute in integer
 */
export const minuteToString = (minute) => {
  const hour = Math.floor(minute / 60)
  minute = minute % 60
  let result = ``
  if (hour) result = `${hour}h`
  if (minute) result = `${result} ${minute}m`
  return result
}

/**
 * format date
 * @param {*} d date string
 */
export const dateString = (d) => {
  const date = new Date(d)
  if (!date) return ''

  const months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'Oktober', 'November', 'Desember']
  return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`
}

/**
 * format number to string and define how many leading zero
 * @param {*} val number in integer
 * @param {*} n how many leading zero
 */
export const leadingZero = function (val, n) {
  n = n || 2
  var isM = val < 0
  var ns = Math.abs(val).toString()
  while (ns.length < n) ns = '0' + ns
  if (isM) ns = '-' + ns
  return ns
}

/**
 * number to Rupiah
*/
export const numberToPrice = (n) => {
  return `Rp${n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-`
}

/**
 * wait for all promise to finish
 * @param {*} promises
 */
export const promiseAll = function (promises) {
  return new Promise((resolve, reject) => {
    const result = []
    let pending
    let i

    if (!promises.length) {
      resolve(result)
      return
    }

    pending = i = promises.length
    while (i--) {
      processPromise(i)
    }

    function processPromise (i) {
      promises[i].then(function (value) {
        result.push(value)

        if (!--pending) {
          resolve(result)
        }
      }, reject)
    }
  })
}

export const isMobile = () => {
  if (window.outerWidth && window.outerWidth < 768) {
    return true
  } else {
    return false
  }
}

export const redirectMobile = (Router, url) => {
  return isMobile() ? Router.push(url) : false
}

/**
 * set interval with immediate call to the function
 * @param func function to stop the interval
*/
export const immediateInterval = function (func, delay = 1000) {
  // called after delay
  const key = window.setInterval(() => {
    func(clear)
  }, delay)

  // immediate call
  func(clear)

  function clear () {
    window.clearInterval(key)
  }

  return key
}

export const isEmpty = function (obj) {
  if (obj === null) return true
  switch (typeof obj) {
  case 'string':
    return obj === ''
  case 'number':
    return obj === 0
  case 'undefined':
    return true
  case 'object':
    if (obj instanceof Array) {
      return !obj.length
    } else if (obj instanceof Map || obj instanceof Set) {
      return !obj.size
    } else {
      return !Object.keys(obj).length
    }
  default:
    return true
  }
}
