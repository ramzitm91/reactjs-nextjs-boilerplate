import Router from 'next/router'

export const getQuery = () => {
  const query = {}
  if (typeof window === 'undefined') {
    return query
  }

  const searchParams = new URLSearchParams(Router.asPath.split(/\?/)[1])
  for (const [key, value] of searchParams) {
    query[key] = value
  }
  return query
}
