/**
 * fetch is just a set of functions that wrap axios
 * the main purpose is to make it easier to create axios function without setting default baseUrl and headers
 * it will automatically set authorization header when it is exist on local storage
 */

import axios from 'axios'
import { groupLogger } from './logger'

axios.defaults.headers.common['accept'] = 'application/json'
axios.defaults.headers.post['content-type'] = 'application/json'

/**
 * read window local storage to get bearer token
 */
export const getAuthHeader = () => {
  const token = window.localStorage.getItem('bo_bearer')
  if (token) return `Bearer ${token}`
  return null
}

/**
 * use interceptor to add header authorization
 */
axios.interceptors.request.use(function (config) {
  const authHeader = getAuthHeader()
  if (authHeader) config.headers['authorization'] = getAuthHeader()
  config.when = window.performance.now()
  return config
}, function (error) {
  return Promise.reject(error)
})
axios.interceptors.response.use(function (response) {
  logReponse(response)
  return response
}, function (error) {
  logReponse(error.response)
  return Promise.reject(error)
})

export const logReponse = (response) => {
  if (!response) return null
  const { config } = response
  const responseTime = window.performance.now() - config.when
  const baseUrl = config.url.replace(process.env.API_BASE_URL, '')
  groupLogger(`${config.method.toUpperCase()} ${baseUrl}`, {
    responseTime,
    status: response.status,
    statusText: response.statusText,
    errorMessage: response.data?.message || ''
  })
}

/**
 * simplify get request by adding base url and query on second parameter
 */
export const get = (path, params) => {
  return axios.get(`${process.env.API_BASE_URL}${path}`, {
    params
  })
}

/**
 * simplify post request by adding base url
 */
export const post = (path, body) => {
  return axios.post(`${process.env.API_BASE_URL}${path}`, body)
}

/**
 * simplify put request by adding base url
 */
export const put = (path, body) => {
  return axios.put(`${process.env.API_BASE_URL}${path}`, body)
}

/**
 * simplify del request by adding base url and query parameter
 */
export const del = (path, params) => {
  return axios.delete(`${process.env.API_BASE_URL}${path}`, {
    params
  })
}

export const defaultError = {
  code: 400,
  message: 'Terjadi gangguan. Tunggu beberapa saat lagi, atau periksa koneksi internet Anda.'
}
