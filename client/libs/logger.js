export const groupLogger = (label, logs) => {
  if (process.env.ENV === 'prod') return // skip log
  console.group(label)
  console.table(logs)
  console.groupEnd()
}
