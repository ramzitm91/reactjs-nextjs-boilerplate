import { get, post, put, del, defaultError } from '@client/libs/fetch'

/**
 * @param {var1, var2} data exmplPost datas
 */
export const exmplPost = (data) => {
  return (dispatch) => {
    return post(`/api/path`, data).then((res) => {
      if (res.status === 200) {
        dispatch({ type: 'SET_ACTION_NAME', data: res.data })
      }
      return res.data
    }, (err) => {
      return err?.response?.data || defaultError
    })
  }
}

export const exmplGet = () => {
  return (dispatch) => {
    return get(`/api/path`).then((res) => {
      if (res.status === 200) {
        dispatch({ type: 'SET_ACTION_NAME', data: res.data })
      }
      return res.data
    }, (err) => {
      return err?.response?.data || defaultError
    })
  }
}

export const exmplDispatch = () => {
  return (dispatch) => {
    dispatch({ type: 'SET_ACTION_NAME' })
  }
}

/**
 * @param {var} params exmplPut parameters
 * @param {var1, var2} data exmplPut datas
 */
export const exmplPut = (params, data) => {
  return (dispatch) => {
    return put(`/api/path/${params}`, data).then((res) => {
      if (res.status === 200) {
        dispatch({ type: 'SET_ACTION_NAME', data: res.data })
      }
      return res.data
    }, (err) => {
      return err?.response?.data || defaultError
    })
  }
}

/**
 * @param {var} params exmplDel parameters
 */
export const exmplDel = (params) => {
  return (dispatch) => {
    return del(`/api/path/${params}`).then(
      (res) => {
        return res.data
      },
      (err, bs) => {
        return err?.response?.data || defaultError
      }
    )
  }
}
