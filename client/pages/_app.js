import React from 'react'
import Head from 'next/head'
import App from 'next/app'
import 'swiper/css/swiper.css'
import '../style.css'
import { initializeStore } from '@client/store'
import { Provider } from 'react-redux'

import Router from 'next/router'

const __NEXT_REDUX_STORE__ = '__NEXT_REDUX_STORE__'
function getOrCreateStore () {
  const initialState = {}

  const isServer = typeof window === 'undefined'
  // Always make a new store if server, otherwise state is shared between requests
  if (isServer) {
    return initializeStore(initialState)
  }

  // Create store if unavailable on the client and set it on the window object
  if (!window[__NEXT_REDUX_STORE__]) {
    window[__NEXT_REDUX_STORE__] = initializeStore(initialState)
  }
  return window[__NEXT_REDUX_STORE__]
}

export default class BioskopOnline extends App {
  constructor (props) {
    super(props)
    this.reduxStore = getOrCreateStore()
  }

  componentDidMount () {
    const bearer = window.localStorage.getItem('bo_bearer')
    if (bearer) {
      this.reduxStore.dispatch({ type: 'SET_LOGIN', data: { data: { token: bearer } } })
    }
    // dispatch event if route exist
    const hashId = Router.asPath.split('#').pop()
    if (hashId) window.dispatchEvent(new window.CustomEvent(hashId, {}))
  }

  render () {
    const { Component, pageProps } = this.props
    return (
      <React.Fragment>
        <Provider store={this.reduxStore}>
          <Head>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <title>react-nextjs-boilerplate</title>
          </Head>
          <Component {...pageProps} />
        </Provider>
      </React.Fragment>
    )
  }
}
