import React from 'react'
import Styled from 'styled-components'

const Page = Styled.div`
  h1 {
    text-align: center;
  }
`

class HomePage extends React.Component {
  render () {
    return <Page>
      <h1>REACT JS NEXT JS BOILERPLATE</h1>
    </Page>
  }
}

export default HomePage
