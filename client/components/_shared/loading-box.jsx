import React from 'react'
import Styled from 'styled-components'

const Loading = Styled.div`
  background: linear-gradient(303deg, #111111, #444444);
  background-size: 400% 400%;

  -webkit-animation: bg-empty 5s ease infinite;
  -moz-animation: bg-empty 5s ease infinite;
  animation: bg-empty 5s ease infinite;

  @-webkit-keyframes bg-empty {
      0% { background-position: 4% 0% }
      50% { background-position: 97% 100% }
      100% { background-position: 4% 0% }
  }
  @-moz-keyframes bg-empty {
      0% { background-position: 4% 0% }
      50% { background-position: 97% 100% }
      100% { background-position: 4% 0% }
  }
  @keyframes bg-empty {
      0% { background-position: 4% 0% }
      50% { background-position: 97% 100% }
      100% { background-position: 4% 0% }
  }
`

export default function LoadingBoxComponent ({ width = '100%', height = '100%', style = {} }) {
  return (
    <Loading style={{ width, height, ...style }}></Loading>
  )
}
