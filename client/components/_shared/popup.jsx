import React from 'react'
import Styled from 'styled-components'

const full = `
  left: 0;
  bottom: 0;
  width: 100vw;
  min-height: 100vh;
  overflow: auto;
`
const Wrapper = Styled.div`
  position: fixed;
  z-index: 1001;
  ${full}
  background-color: rgba(6, 16, 42, 0.7);
  .popup-container {
    position: absolute;
    left: 50%;
    top: 10%;
    transform: translate(-50%, 0);
    background-color: rgba(0, 0, 0, .85);
    border-radius: 1rem;
    margin-bottom: 2rem;
    @media screen and (min-width: 768px) {
      top: 50%;
      transform: translate(-50%, -50%);
    }
  }
  .close-bg {
    position: absolute;
    ${full}
  }
`

export default function Popup ({ children, style = {}, onClose = () => {}, disableClose = false }) {
  return (
    <Wrapper>
      <div className='close-bg' onClick={!disableClose ? onClose : () => {}}></div>
      <div className='popup-container'>
        { children }
      </div>
    </Wrapper>
  )
}
