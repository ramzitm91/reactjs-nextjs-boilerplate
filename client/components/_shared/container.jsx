import React from 'react'
import Styled from 'styled-components'

const Container = Styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  box-sizing: border-box;
  @media (min-width: 1250px) {
    max-width: 1250px;
  }
`
export default function SContainer ({ noPadding, children }) {
  return (
    <Container className="container" style={{ paddingLeft: `${noPadding ? 0 : 15}px`, paddingRight: `${noPadding ? 0 : 15}px` }} >
      { children }
    </Container>
  )
}
