import React from 'react'
import Styled from 'styled-components'

const Wrapper = Styled.div`
  display: flex;
  justify-content: center;
  &.screen {
    width: 100vw;
    height: 300px;
    align-items: center;
  }
`
const Loading = Styled.div`
  width: ${({ size }) => `${size}px`};
  height: ${({ size }) => `${size / 5}px`};
  display: flex;
  justify-content: space-between;
  .rect {
    background-color: ${({ color }) => color || '#ffffff'};
    height: ${({ size }) => `${size / 5}px`};
    width: ${({ size }) => `${size / 5}px`};
    animation: wave 1.2s infinite ease-in-out;
    border-radius: 50%;
    &:nth-child(1) { animation-delay: -1.2s; }
    &:nth-child(2) { animation-delay: -1.1s; }
    &:nth-child(3) { animation-delay: -1.0s; }
    &:nth-child(4) { animation-delay: -0.9s; }
    &:nth-child(5) { animation-delay: -0.8s; }
  }
  @keyframes wave {
    0%, 40%, 100% {
      transform: scale(0.4); 
    } 20% {
      transform: scale(1); 
    } 
  }
`

export default function LoadingComponent ({ size = 100, color, mode = '' }) {
  return (
    <Wrapper className={`loading ${mode}`}>
      <Loading size={size} color={color}>
        <div className='rect'></div>
        <div className='rect'></div>
        <div className='rect'></div>
        <div className='rect'></div>
        <div className='rect'></div>
      </Loading>
    </Wrapper>
  )
}
