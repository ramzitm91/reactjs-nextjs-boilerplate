const path = require('path')
const Dotenv = require('dotenv-webpack')

const alias = {
  '@client': path.join(__dirname, 'client'),
  '@components': path.join(__dirname, 'client', 'components'),
  '@containers': path.join(__dirname, 'client', 'containers')
}

module.exports = {
  webpack: (config) => {
    config.plugins = config.plugins || []
    config.plugins = [
      ...config.plugins,
      // Read the .env file
      new Dotenv({
        path: path.join(__dirname, '.env'),
        systemvars: true
      })
    ]

    config.resolve.alias = Object.assign(
      {},
      config.resolve.alias,
      alias
    )

    return config
  }
}
